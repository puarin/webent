/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webapp.model;

import com.webapp.dao.ManagerDataAccess;
import com.webapp.entities.Post;
import java.util.List;

/**
 *
 * @author Puarinnnn
 */
public class PostManagerBean {
    private Post post;
    private final ManagerDataAccess dataAccess;

    public PostManagerBean() {
        this.dataAccess = new ManagerDataAccess();
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public boolean addPost(){
        return dataAccess.addPost(post);
    }
    
    public List<Post> getUserPosts(){
        return dataAccess.getUserPosts();
    }
}
