/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webapp.dao;

import com.webapp.entities.Comment;
import com.webapp.entities.Post;
import com.webapp.entities.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Puarinnnn
 */
public class ManagerDataAccess {
    ConnectDB connectDB = new ConnectDB();

    public boolean addUser(User user) {
        try {
            String query = "  INSERT INTO dbo.dboUSER( username ,password ,avatar ,name ,age ,role)"
                    + "VALUES(?,?,?,?,?,'USER')";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getAvatar());
            ps.setString(4, user.getName());
            ps.setInt(5, user.getAge());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean addPost(Post post) {
        try {
            String query = "INSERT INTO dboPOST(username,content,picture) VALUES(?,?,?)";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, post.getUsername());
            ps.setString(2, post.getContent());
            ps.setString(3, post.getPicture());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean addComment(Comment comment) {
        try {
            String query = "INSERT INTO dboCOMMENT(username,postid,content,picture) VALUES(?,?,?,?)";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, comment.getUsername());
            ps.setInt(2, comment.getPostid());
            ps.setString(3, comment.getContent());
            ps.setString(4, comment.getPicture());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public String Login(User user) {
        try {
            String query = "SELECT * FROM dboUSER WHERE username = ? AND password = ?";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ResultSet rs = ps.executeQuery();
            String role = null;
            while (rs.next()) {
                role = rs.getString("role");
            }
            return role;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Post> getUserPosts() {
        List<Post> posts = new LinkedList<>();
        try {
            String query = "SELECT * FROM dboPOST ORDER BY id DESC";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"), rs.getString("username"), rs.getString("content"), rs.getString("picture"), rs.getTimestamp("date_create")));
            }
            
            return posts;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Comment> getCommentsPostID(String post_id) {
        List<Comment> comments = new LinkedList<>();
        try {
            String query = "SELECT * FROM dboCOMMENT WHERE postid = ? ORDER BY id DESC";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, post_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                comments.add(new Comment(rs.getInt("id"),rs.getString("username"),rs.getInt("postid"), rs.getString("content"), rs.getString("picture"), rs.getTimestamp("date_create")));
            }
            return comments;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public User getUserByAcc(String acc) {
        User user = null;
        try {
            String query = "SELECT * FROM dboUSER WHERE username = ?";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, acc);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new User(rs.getString("name"), rs.getString("username"), null, rs.getString("avatar"), rs.getInt("age"), rs.getString("role"));
            }
            return user;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public boolean updateUser(User user) {
        try {
            String query = "UPDATE dboUSER SET avatar = ?, name = ?, age = ? WHERE username = ?";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getAvatar());
            ps.setString(2, user.getName());      
            ps.setInt(3, user.getAge());
            ps.setString(4, user.getUsername());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public String checkAdmin(String acc) {
        try {
            String query = "SELECT role FROM dboUSER WHERE username = ?";
            Connection connection = connectDB.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, acc);
            ResultSet rs = ps.executeQuery();
            String role = null;
            while (rs.next()) {
                role = rs.getString("role");
            }
            return role;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
