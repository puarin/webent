/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webapp.entities;

import java.sql.Timestamp;

/**
 *
 * @author Puarinnnn
 */
public class User {
    private String name;
    private String username;
    private String password;
    private String avatar;
    private int age;
    private String role;

    public User() {
    }

    public User(String name, String username, String password, String avatar, int age, String role) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.avatar = avatar;
        this.age = age;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    
    
}
