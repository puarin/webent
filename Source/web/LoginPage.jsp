<%-- 
    Document   : loginPage
    Created on : Apr 19, 2018, 5:34:59 AM
    Author     : Puarinnnn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        <h1>Login page</h1>
        <form action="Login" method="post">
            <table border="1">
                <tr>
                    <td>Account</td>
                    <td><input name="acc"/><br/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="pass"/><br/></td>
                </tr>
                <tr>
                    <td colspan ="2">
                    <input type="submit" value="login"/>
                    </td>
                </tr>             
            </table>
        </form>
        <br>
        ${msg_failed}
    </body>
</html>
