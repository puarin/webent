<%-- 
    Document   : ViewProfilePage
    Created on : Apr 19, 2018, 5:34:59 AM
    Author     : Puarinnnn
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Welcome,</h3>
        <a href="ViewProfile">${exist_acc}</a>
        <c:if test="${is_admin eq true}">
            (ADMIN)
        </c:if>

        <c:if test="${is_admin eq true}">
            <a href="AddUser.jsp">
                Create user
            </a>
        </c:if>
        <a href="Logout">
            Logout
        </a><br/>
        <br/>
        <form action="UpdateProfile" method="post" enctype="multipart/form-data">      
            <table>
                <tr>
                    <td>Your name</td>
                    <td><input name="name" value="${user.name}"/>    </td>
                </tr>
                <tr>
                    <td>Age</td>
                    <td><input name="age" value="${user.age}"/></td>
                </tr>
                <tr>
                    <td>Avatar</td>
                    <td><c:if test="${not empty user.avatar}">
                    <img height="150" width="200" src="images/users/${user.avatar}"/>
                </c:if>
                <input name="photo" type="file"/></td>
                </tr>
                <tr>
                    <td colspan="20"><button type="submit">Update</button></td>
                </tr>           
            </table>
        </form>

    </body>
</html>
